module gitlab.com/tfournier/wey

go 1.15

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/ajstarks/svgo v0.0.0-20200725142600-7a3c8b57fecb
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/boombuler/barcode v1.0.0
	github.com/brutella/hc v1.2.3
	github.com/jinzhu/configor v1.2.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.9
	gitlab.com/tfournier/ipx800v4 v0.1.0
	golang.org/x/sys v0.0.0-20201029080932-201ba4db2418 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
