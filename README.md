# Wey

![logo](docs/logo_landscape.png)

*Wey is a [GCE Electronics](https://www.gce-electronics.com) IPX800 ([version 4](https://www.gce-electronics.com/fr/carte-relais-ethernet-module-rail-din/1483-domotique-ethernet-webserver-ipx800-v4-3760309690001.html)) gateway to control your home with [Apple HomeKit](https://www.apple.com/ios/home/)*

## Gateway installation
1. Download last [release](https://gitlab.com/tfournier/wey/-/releases)
2. Install package `apt install  ./wey_VERSION_OS_ARCH.deb`
3. Go to http://127.0.0.1
