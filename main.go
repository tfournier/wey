package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/tfournier/wey/internal"
)

func main() {
	debug := flag.Bool("debug", false, "Debug mode")
	flag.Parse()

	gw, err := internal.New(*debug)
	if err != nil {
		log.Println(err)

		if p, err := os.FindProcess(os.Getpid()); err == nil {
			_ = p.Signal(os.Interrupt)
		}
	}

	gw.Run()
}
