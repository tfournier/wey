package ipx

import "fmt"

type Relay interface {
	ID() int
	Name() string
	Value() bool
	Set() error
	Clear() error
	Toggle() error
}

type relay struct {
	id    int
	value bool
	api   API
}

func (r relay) ID() int {
	return r.id
}

func (r relay) Name() string {
	return fmt.Sprintf("R%0.2d", r.id)
}

func (r relay) Value() bool {
	return r.value
}

func (r relay) Set() error {
	_, err := r.api.GET(map[string]interface{}{"SetR": fmt.Sprintf("%0.2d", r.id)})
	if err != nil {
		return err
	}

	return nil
}

func (r relay) Clear() error {
	_, err := r.api.GET(map[string]interface{}{"ClearR": fmt.Sprintf("%0.2d", r.id)})
	if err != nil {
		return err
	}

	return nil
}

func (r relay) Toggle() error {
	_, err := r.api.GET(map[string]interface{}{"ToggleR": fmt.Sprintf("%0.2d", r.id)})
	if err != nil {
		return err
	}

	return nil
}
