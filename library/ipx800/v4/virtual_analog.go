package ipx

import "fmt"

type VirtualAnalog interface {
	ID() int
	Name() string
	Value() int
	Set(value int) error
}

type virtualAnalog struct {
	id    int
	value int
	api   API
}

func (v virtualAnalog) ID() int {
	return v.id
}

func (v virtualAnalog) Name() string {
	return fmt.Sprintf("VA%0.3d", v.id)
}

func (v virtualAnalog) Value() int {
	return v.value
}

func (v virtualAnalog) Set(value int) error {
	_, err := v.api.GET(map[string]interface{}{fmt.Sprintf("SetVA%0.2d", v.id): value})
	if err != nil {
		return err
	}

	return nil
}
