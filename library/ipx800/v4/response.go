package ipx

type Response struct {
	Product    *string `json:"product,omitempty" xml:"-"`
	Status     *string `json:"status,omitempty" xml:"-"`
	MacAddress *string `json:"-" xml:"mac,omitempty"`

	Relay01 *int `json:"R1,omitempty" xml:"led0,omitempty"`
	Relay02 *int `json:"R2,omitempty" xml:"led1,omitempty"`
	Relay03 *int `json:"R3,omitempty" xml:"led2,omitempty"`
	Relay04 *int `json:"R4,omitempty" xml:"led3,omitempty"`
	Relay05 *int `json:"R5,omitempty" xml:"led4,omitempty"`
	Relay06 *int `json:"R6,omitempty" xml:"led5,omitempty"`
	Relay07 *int `json:"R7,omitempty" xml:"led6,omitempty"`
	Relay08 *int `json:"R8,omitempty" xml:"led7,omitempty"`
	Relay09 *int `json:"R9,omitempty" xml:"led8,omitempty"`
	Relay10 *int `json:"R10,omitempty" xml:"led9,omitempty"`
	Relay11 *int `json:"R11,omitempty" xml:"led10,omitempty"`
	Relay12 *int `json:"R12,omitempty" xml:"led11,omitempty"`
	Relay13 *int `json:"R13,omitempty" xml:"led12,omitempty"`
	Relay14 *int `json:"R14,omitempty" xml:"led13,omitempty"`
	Relay15 *int `json:"R15,omitempty" xml:"led14,omitempty"`
	Relay16 *int `json:"R16,omitempty" xml:"led15,omitempty"`
	Relay17 *int `json:"R17,omitempty" xml:"led16,omitempty"`
	Relay18 *int `json:"R18,omitempty" xml:"led17,omitempty"`
	Relay19 *int `json:"R19,omitempty" xml:"led18,omitempty"`
	Relay20 *int `json:"R20,omitempty" xml:"led19,omitempty"`
	Relay21 *int `json:"R21,omitempty" xml:"led20,omitempty"`
	Relay22 *int `json:"R22,omitempty" xml:"led21,omitempty"`
	Relay23 *int `json:"R23,omitempty" xml:"led22,omitempty"`
	Relay24 *int `json:"R24,omitempty" xml:"led23,omitempty"`
	Relay25 *int `json:"R25,omitempty" xml:"led24,omitempty"`
	Relay26 *int `json:"R26,omitempty" xml:"led25,omitempty"`
	Relay27 *int `json:"R27,omitempty" xml:"led26,omitempty"`
	Relay28 *int `json:"R28,omitempty" xml:"led27,omitempty"`
	Relay29 *int `json:"R29,omitempty" xml:"led28,omitempty"`
	Relay30 *int `json:"R30,omitempty" xml:"led29,omitempty"`
	Relay31 *int `json:"R31,omitempty" xml:"led30,omitempty"`
	Relay32 *int `json:"R32,omitempty" xml:"led31,omitempty"`
	Relay33 *int `json:"R33,omitempty" xml:"led32,omitempty"`
	Relay34 *int `json:"R34,omitempty" xml:"led33,omitempty"`
	Relay35 *int `json:"R35,omitempty" xml:"led34,omitempty"`
	Relay36 *int `json:"R36,omitempty" xml:"led35,omitempty"`
	Relay37 *int `json:"R37,omitempty" xml:"led36,omitempty"`
	Relay38 *int `json:"R38,omitempty" xml:"led37,omitempty"`
	Relay39 *int `json:"R39,omitempty" xml:"led38,omitempty"`
	Relay40 *int `json:"R40,omitempty" xml:"led39,omitempty"`
	Relay41 *int `json:"R41,omitempty" xml:"led40,omitempty"`
	Relay42 *int `json:"R42,omitempty" xml:"led41,omitempty"`
	Relay43 *int `json:"R43,omitempty" xml:"led42,omitempty"`
	Relay44 *int `json:"R44,omitempty" xml:"led43,omitempty"`
	Relay45 *int `json:"R45,omitempty" xml:"led44,omitempty"`
	Relay46 *int `json:"R46,omitempty" xml:"led45,omitempty"`
	Relay47 *int `json:"R47,omitempty" xml:"led46,omitempty"`
	Relay48 *int `json:"R48,omitempty" xml:"led47,omitempty"`
	Relay49 *int `json:"R49,omitempty" xml:"led48,omitempty"`
	Relay50 *int `json:"R50,omitempty" xml:"led49,omitempty"`
	Relay51 *int `json:"R51,omitempty" xml:"led50,omitempty"`
	Relay52 *int `json:"R52,omitempty" xml:"led51,omitempty"`
	Relay53 *int `json:"R53,omitempty" xml:"led52,omitempty"`
	Relay54 *int `json:"R54,omitempty" xml:"led53,omitempty"`
	Relay55 *int `json:"R55,omitempty" xml:"led54,omitempty"`
	Relay56 *int `json:"R56,omitempty" xml:"led55,omitempty"`

	Digital01 *int `json:"D1,omitempty" xml:"-"`
	Digital02 *int `json:"D2,omitempty" xml:"-"`
	Digital03 *int `json:"D3,omitempty" xml:"-"`
	Digital04 *int `json:"D4,omitempty" xml:"-"`
	Digital05 *int `json:"D5,omitempty" xml:"-"`
	Digital06 *int `json:"D6,omitempty" xml:"-"`
	Digital07 *int `json:"D7,omitempty" xml:"-"`
	Digital08 *int `json:"D8,omitempty" xml:"-"`
	Digital09 *int `json:"D9,omitempty" xml:"-"`
	Digital10 *int `json:"D10,omitempty" xml:"-"`
	Digital11 *int `json:"D11,omitempty" xml:"-"`
	Digital12 *int `json:"D12,omitempty" xml:"-"`
	Digital13 *int `json:"D13,omitempty" xml:"-"`
	Digital14 *int `json:"D14,omitempty" xml:"-"`
	Digital15 *int `json:"D15,omitempty" xml:"-"`
	Digital16 *int `json:"D16,omitempty" xml:"-"`
	Digital17 *int `json:"D17,omitempty" xml:"-"`
	Digital18 *int `json:"D18,omitempty" xml:"-"`
	Digital19 *int `json:"D19,omitempty" xml:"-"`
	Digital20 *int `json:"D20,omitempty" xml:"-"`
	Digital21 *int `json:"D21,omitempty" xml:"-"`
	Digital22 *int `json:"D22,omitempty" xml:"-"`
	Digital23 *int `json:"D23,omitempty" xml:"-"`
	Digital24 *int `json:"D24,omitempty" xml:"-"`
	Digital25 *int `json:"D25,omitempty" xml:"-"`
	Digital26 *int `json:"D26,omitempty" xml:"-"`
	Digital27 *int `json:"D27,omitempty" xml:"-"`
	Digital28 *int `json:"D28,omitempty" xml:"-"`
	Digital29 *int `json:"D29,omitempty" xml:"-"`
	Digital30 *int `json:"D30,omitempty" xml:"-"`
	Digital31 *int `json:"D31,omitempty" xml:"-"`
	Digital32 *int `json:"D32,omitempty" xml:"-"`
	Digital33 *int `json:"D33,omitempty" xml:"-"`
	Digital34 *int `json:"D34,omitempty" xml:"-"`
	Digital35 *int `json:"D35,omitempty" xml:"-"`
	Digital36 *int `json:"D36,omitempty" xml:"-"`
	Digital37 *int `json:"D37,omitempty" xml:"-"`
	Digital38 *int `json:"D38,omitempty" xml:"-"`
	Digital39 *int `json:"D39,omitempty" xml:"-"`
	Digital40 *int `json:"D40,omitempty" xml:"-"`
	Digital41 *int `json:"D41,omitempty" xml:"-"`
	Digital42 *int `json:"D42,omitempty" xml:"-"`
	Digital43 *int `json:"D43,omitempty" xml:"-"`
	Digital44 *int `json:"D44,omitempty" xml:"-"`
	Digital45 *int `json:"D45,omitempty" xml:"-"`
	Digital46 *int `json:"D46,omitempty" xml:"-"`
	Digital47 *int `json:"D47,omitempty" xml:"-"`
	Digital48 *int `json:"D48,omitempty" xml:"-"`
	Digital49 *int `json:"D49,omitempty" xml:"-"`
	Digital50 *int `json:"D50,omitempty" xml:"-"`
	Digital51 *int `json:"D51,omitempty" xml:"-"`
	Digital52 *int `json:"D52,omitempty" xml:"-"`
	Digital53 *int `json:"D53,omitempty" xml:"-"`
	Digital54 *int `json:"D54,omitempty" xml:"-"`
	Digital55 *int `json:"D55,omitempty" xml:"-"`
	Digital56 *int `json:"D56,omitempty" xml:"-"`

	Analog1 *int `json:"A1,omitempty" xml:"analog0,omitempty"`
	Analog2 *int `json:"A2,omitempty" xml:"analog1,omitempty"`
	Analog3 *int `json:"A3,omitempty" xml:"analog2,omitempty"`
	Analog4 *int `json:"A4,omitempty" xml:"analog3,omitempty"`

	VirtualOutput001 *int `json:"VO1,omitempty" xml:"vout0,omitempty"`
	VirtualOutput002 *int `json:"VO2,omitempty" xml:"vout1,omitempty"`
	VirtualOutput003 *int `json:"VO3,omitempty" xml:"vout2,omitempty"`
	VirtualOutput004 *int `json:"VO4,omitempty" xml:"vout3,omitempty"`
	VirtualOutput005 *int `json:"VO5,omitempty" xml:"vout4,omitempty"`
	VirtualOutput006 *int `json:"VO6,omitempty" xml:"vout5,omitempty"`
	VirtualOutput007 *int `json:"VO7,omitempty" xml:"vout6,omitempty"`
	VirtualOutput008 *int `json:"VO8,omitempty" xml:"vout7,omitempty"`
	VirtualOutput009 *int `json:"VO9,omitempty" xml:"vout8,omitempty"`
	VirtualOutput010 *int `json:"VO10,omitempty" xml:"vout9,omitempty"`
	VirtualOutput011 *int `json:"VO11,omitempty" xml:"vout10,omitempty"`
	VirtualOutput012 *int `json:"VO12,omitempty" xml:"vout11,omitempty"`
	VirtualOutput013 *int `json:"VO13,omitempty" xml:"vout12,omitempty"`
	VirtualOutput014 *int `json:"VO14,omitempty" xml:"vout13,omitempty"`
	VirtualOutput015 *int `json:"VO15,omitempty" xml:"vout14,omitempty"`
	VirtualOutput016 *int `json:"VO16,omitempty" xml:"vout15,omitempty"`
	VirtualOutput017 *int `json:"VO17,omitempty" xml:"vout16,omitempty"`
	VirtualOutput018 *int `json:"VO18,omitempty" xml:"vout17,omitempty"`
	VirtualOutput019 *int `json:"VO19,omitempty" xml:"vout18,omitempty"`
	VirtualOutput020 *int `json:"VO20,omitempty" xml:"vout19,omitempty"`
	VirtualOutput021 *int `json:"VO21,omitempty" xml:"vout20,omitempty"`
	VirtualOutput022 *int `json:"VO22,omitempty" xml:"vout21,omitempty"`
	VirtualOutput023 *int `json:"VO23,omitempty" xml:"vout22,omitempty"`
	VirtualOutput024 *int `json:"VO24,omitempty" xml:"vout23,omitempty"`
	VirtualOutput025 *int `json:"VO25,omitempty" xml:"vout24,omitempty"`
	VirtualOutput026 *int `json:"VO26,omitempty" xml:"vout25,omitempty"`
	VirtualOutput027 *int `json:"VO27,omitempty" xml:"vout26,omitempty"`
	VirtualOutput028 *int `json:"VO28,omitempty" xml:"vout27,omitempty"`
	VirtualOutput029 *int `json:"VO29,omitempty" xml:"vout28,omitempty"`
	VirtualOutput030 *int `json:"VO30,omitempty" xml:"vout29,omitempty"`
	VirtualOutput031 *int `json:"VO31,omitempty" xml:"vout30,omitempty"`
	VirtualOutput032 *int `json:"VO32,omitempty" xml:"vout31,omitempty"`
	VirtualOutput033 *int `json:"VO33,omitempty" xml:"vout32,omitempty"`
	VirtualOutput034 *int `json:"VO34,omitempty" xml:"vout33,omitempty"`
	VirtualOutput035 *int `json:"VO35,omitempty" xml:"vout34,omitempty"`
	VirtualOutput036 *int `json:"VO36,omitempty" xml:"vout35,omitempty"`
	VirtualOutput037 *int `json:"VO37,omitempty" xml:"vout36,omitempty"`
	VirtualOutput038 *int `json:"VO38,omitempty" xml:"vout37,omitempty"`
	VirtualOutput039 *int `json:"VO39,omitempty" xml:"vout38,omitempty"`
	VirtualOutput040 *int `json:"VO40,omitempty" xml:"vout39,omitempty"`
	VirtualOutput041 *int `json:"VO41,omitempty" xml:"vout40,omitempty"`
	VirtualOutput042 *int `json:"VO42,omitempty" xml:"vout41,omitempty"`
	VirtualOutput043 *int `json:"VO43,omitempty" xml:"vout42,omitempty"`
	VirtualOutput044 *int `json:"VO44,omitempty" xml:"vout43,omitempty"`
	VirtualOutput045 *int `json:"VO45,omitempty" xml:"vout44,omitempty"`
	VirtualOutput046 *int `json:"VO46,omitempty" xml:"vout45,omitempty"`
	VirtualOutput047 *int `json:"VO47,omitempty" xml:"vout46,omitempty"`
	VirtualOutput048 *int `json:"VO48,omitempty" xml:"vout47,omitempty"`
	VirtualOutput049 *int `json:"VO49,omitempty" xml:"vout48,omitempty"`
	VirtualOutput050 *int `json:"VO50,omitempty" xml:"vout49,omitempty"`
	VirtualOutput051 *int `json:"VO51,omitempty" xml:"vout50,omitempty"`
	VirtualOutput052 *int `json:"VO52,omitempty" xml:"vout51,omitempty"`
	VirtualOutput053 *int `json:"VO53,omitempty" xml:"vout52,omitempty"`
	VirtualOutput054 *int `json:"VO54,omitempty" xml:"vout53,omitempty"`
	VirtualOutput055 *int `json:"VO55,omitempty" xml:"vout54,omitempty"`
	VirtualOutput056 *int `json:"VO56,omitempty" xml:"vout55,omitempty"`
	VirtualOutput057 *int `json:"VO57,omitempty" xml:"vout56,omitempty"`
	VirtualOutput058 *int `json:"VO58,omitempty" xml:"vout57,omitempty"`
	VirtualOutput059 *int `json:"VO59,omitempty" xml:"vout58,omitempty"`
	VirtualOutput060 *int `json:"VO60,omitempty" xml:"vout59,omitempty"`
	VirtualOutput061 *int `json:"VO61,omitempty" xml:"vout60,omitempty"`
	VirtualOutput062 *int `json:"VO62,omitempty" xml:"vout61,omitempty"`
	VirtualOutput063 *int `json:"VO63,omitempty" xml:"vout62,omitempty"`
	VirtualOutput064 *int `json:"VO64,omitempty" xml:"vout63,omitempty"`
	VirtualOutput065 *int `json:"VO65,omitempty" xml:"vout64,omitempty"`
	VirtualOutput066 *int `json:"VO66,omitempty" xml:"vout65,omitempty"`
	VirtualOutput067 *int `json:"VO67,omitempty" xml:"vout66,omitempty"`
	VirtualOutput068 *int `json:"VO68,omitempty" xml:"vout67,omitempty"`
	VirtualOutput069 *int `json:"VO69,omitempty" xml:"vout68,omitempty"`
	VirtualOutput070 *int `json:"VO70,omitempty" xml:"vout69,omitempty"`
	VirtualOutput071 *int `json:"VO71,omitempty" xml:"vout70,omitempty"`
	VirtualOutput072 *int `json:"VO72,omitempty" xml:"vout71,omitempty"`
	VirtualOutput073 *int `json:"VO73,omitempty" xml:"vout72,omitempty"`
	VirtualOutput074 *int `json:"VO74,omitempty" xml:"vout73,omitempty"`
	VirtualOutput075 *int `json:"VO75,omitempty" xml:"vout74,omitempty"`
	VirtualOutput076 *int `json:"VO76,omitempty" xml:"vout75,omitempty"`
	VirtualOutput077 *int `json:"VO77,omitempty" xml:"vout76,omitempty"`
	VirtualOutput078 *int `json:"VO78,omitempty" xml:"vout77,omitempty"`
	VirtualOutput079 *int `json:"VO79,omitempty" xml:"vout78,omitempty"`
	VirtualOutput080 *int `json:"VO80,omitempty" xml:"vout79,omitempty"`
	VirtualOutput081 *int `json:"VO81,omitempty" xml:"vout80,omitempty"`
	VirtualOutput082 *int `json:"VO82,omitempty" xml:"vout81,omitempty"`
	VirtualOutput083 *int `json:"VO83,omitempty" xml:"vout82,omitempty"`
	VirtualOutput084 *int `json:"VO84,omitempty" xml:"vout83,omitempty"`
	VirtualOutput085 *int `json:"VO85,omitempty" xml:"vout84,omitempty"`
	VirtualOutput086 *int `json:"VO86,omitempty" xml:"vout85,omitempty"`
	VirtualOutput087 *int `json:"VO87,omitempty" xml:"vout86,omitempty"`
	VirtualOutput088 *int `json:"VO88,omitempty" xml:"vout87,omitempty"`
	VirtualOutput089 *int `json:"VO89,omitempty" xml:"vout88,omitempty"`
	VirtualOutput090 *int `json:"VO90,omitempty" xml:"vout89,omitempty"`
	VirtualOutput091 *int `json:"VO91,omitempty" xml:"vout90,omitempty"`
	VirtualOutput092 *int `json:"VO92,omitempty" xml:"vout91,omitempty"`
	VirtualOutput093 *int `json:"VO93,omitempty" xml:"vout92,omitempty"`
	VirtualOutput094 *int `json:"VO94,omitempty" xml:"vout93,omitempty"`
	VirtualOutput095 *int `json:"VO95,omitempty" xml:"vout94,omitempty"`
	VirtualOutput096 *int `json:"VO96,omitempty" xml:"vout95,omitempty"`
	VirtualOutput097 *int `json:"VO97,omitempty" xml:"vout96,omitempty"`
	VirtualOutput098 *int `json:"VO98,omitempty" xml:"vout97,omitempty"`
	VirtualOutput099 *int `json:"VO99,omitempty" xml:"vout98,omitempty"`
	VirtualOutput100 *int `json:"VO100,omitempty" xml:"vout99,omitempty"`
	VirtualOutput101 *int `json:"VO101,omitempty" xml:"vout100,omitempty"`
	VirtualOutput102 *int `json:"VO102,omitempty" xml:"vout101,omitempty"`
	VirtualOutput103 *int `json:"VO103,omitempty" xml:"vout102,omitempty"`
	VirtualOutput104 *int `json:"VO104,omitempty" xml:"vout103,omitempty"`
	VirtualOutput105 *int `json:"VO105,omitempty" xml:"vout104,omitempty"`
	VirtualOutput106 *int `json:"VO106,omitempty" xml:"vout105,omitempty"`
	VirtualOutput107 *int `json:"VO107,omitempty" xml:"vout106,omitempty"`
	VirtualOutput108 *int `json:"VO108,omitempty" xml:"vout107,omitempty"`
	VirtualOutput109 *int `json:"VO109,omitempty" xml:"vout108,omitempty"`
	VirtualOutput110 *int `json:"VO110,omitempty" xml:"vout109,omitempty"`
	VirtualOutput111 *int `json:"VO111,omitempty" xml:"vout110,omitempty"`
	VirtualOutput112 *int `json:"VO112,omitempty" xml:"vout111,omitempty"`
	VirtualOutput113 *int `json:"VO113,omitempty" xml:"vout112,omitempty"`
	VirtualOutput114 *int `json:"VO114,omitempty" xml:"vout113,omitempty"`
	VirtualOutput115 *int `json:"VO115,omitempty" xml:"vout114,omitempty"`
	VirtualOutput116 *int `json:"VO116,omitempty" xml:"vout115,omitempty"`
	VirtualOutput117 *int `json:"VO117,omitempty" xml:"vout116,omitempty"`
	VirtualOutput118 *int `json:"VO118,omitempty" xml:"vout117,omitempty"`
	VirtualOutput119 *int `json:"VO119,omitempty" xml:"vout118,omitempty"`
	VirtualOutput120 *int `json:"VO120,omitempty" xml:"vout119,omitempty"`
	VirtualOutput121 *int `json:"VO121,omitempty" xml:"vout120,omitempty"`
	VirtualOutput122 *int `json:"VO122,omitempty" xml:"vout121,omitempty"`
	VirtualOutput123 *int `json:"VO123,omitempty" xml:"vout122,omitempty"`
	VirtualOutput124 *int `json:"VO124,omitempty" xml:"vout123,omitempty"`
	VirtualOutput125 *int `json:"VO125,omitempty" xml:"vout124,omitempty"`
	VirtualOutput126 *int `json:"VO126,omitempty" xml:"vout125,omitempty"`
	VirtualOutput127 *int `json:"VO127,omitempty" xml:"vout126,omitempty"`
	VirtualOutput128 *int `json:"VO128,omitempty" xml:"vout127,omitempty"`

	VirtualInput001 *int `json:"VI1,omitempty" xml:"vin0,omitempty"`
	VirtualInput002 *int `json:"VI2,omitempty" xml:"vin1,omitempty"`
	VirtualInput003 *int `json:"VI3,omitempty" xml:"vin2,omitempty"`
	VirtualInput004 *int `json:"VI4,omitempty" xml:"vin3,omitempty"`
	VirtualInput005 *int `json:"VI5,omitempty" xml:"vin4,omitempty"`
	VirtualInput006 *int `json:"VI6,omitempty" xml:"vin5,omitempty"`
	VirtualInput007 *int `json:"VI7,omitempty" xml:"vin6,omitempty"`
	VirtualInput008 *int `json:"VI8,omitempty" xml:"vin7,omitempty"`
	VirtualInput009 *int `json:"VI9,omitempty" xml:"vin8,omitempty"`
	VirtualInput010 *int `json:"VI10,omitempty" xml:"vin9,omitempty"`
	VirtualInput011 *int `json:"VI11,omitempty" xml:"vin10,omitempty"`
	VirtualInput012 *int `json:"VI12,omitempty" xml:"vin11,omitempty"`
	VirtualInput013 *int `json:"VI13,omitempty" xml:"vin12,omitempty"`
	VirtualInput014 *int `json:"VI14,omitempty" xml:"vin13,omitempty"`
	VirtualInput015 *int `json:"VI15,omitempty" xml:"vin14,omitempty"`
	VirtualInput016 *int `json:"VI16,omitempty" xml:"vin15,omitempty"`
	VirtualInput017 *int `json:"VI17,omitempty" xml:"vin16,omitempty"`
	VirtualInput018 *int `json:"VI18,omitempty" xml:"vin17,omitempty"`
	VirtualInput019 *int `json:"VI19,omitempty" xml:"vin18,omitempty"`
	VirtualInput020 *int `json:"VI20,omitempty" xml:"vin19,omitempty"`
	VirtualInput021 *int `json:"VI21,omitempty" xml:"vin20,omitempty"`
	VirtualInput022 *int `json:"VI22,omitempty" xml:"vin21,omitempty"`
	VirtualInput023 *int `json:"VI23,omitempty" xml:"vin22,omitempty"`
	VirtualInput024 *int `json:"VI24,omitempty" xml:"vin23,omitempty"`
	VirtualInput025 *int `json:"VI25,omitempty" xml:"vin24,omitempty"`
	VirtualInput026 *int `json:"VI26,omitempty" xml:"vin25,omitempty"`
	VirtualInput027 *int `json:"VI27,omitempty" xml:"vin26,omitempty"`
	VirtualInput028 *int `json:"VI28,omitempty" xml:"vin27,omitempty"`
	VirtualInput029 *int `json:"VI29,omitempty" xml:"vin28,omitempty"`
	VirtualInput030 *int `json:"VI30,omitempty" xml:"vin29,omitempty"`
	VirtualInput031 *int `json:"VI31,omitempty" xml:"vin30,omitempty"`
	VirtualInput032 *int `json:"VI32,omitempty" xml:"vin31,omitempty"`
	VirtualInput033 *int `json:"VI33,omitempty" xml:"vin32,omitempty"`
	VirtualInput034 *int `json:"VI34,omitempty" xml:"vin33,omitempty"`
	VirtualInput035 *int `json:"VI35,omitempty" xml:"vin34,omitempty"`
	VirtualInput036 *int `json:"VI36,omitempty" xml:"vin35,omitempty"`
	VirtualInput037 *int `json:"VI37,omitempty" xml:"vin36,omitempty"`
	VirtualInput038 *int `json:"VI38,omitempty" xml:"vin37,omitempty"`
	VirtualInput039 *int `json:"VI39,omitempty" xml:"vin38,omitempty"`
	VirtualInput040 *int `json:"VI40,omitempty" xml:"vin39,omitempty"`
	VirtualInput041 *int `json:"VI41,omitempty" xml:"vin40,omitempty"`
	VirtualInput042 *int `json:"VI42,omitempty" xml:"vin41,omitempty"`
	VirtualInput043 *int `json:"VI43,omitempty" xml:"vin42,omitempty"`
	VirtualInput044 *int `json:"VI44,omitempty" xml:"vin43,omitempty"`
	VirtualInput045 *int `json:"VI45,omitempty" xml:"vin44,omitempty"`
	VirtualInput046 *int `json:"VI46,omitempty" xml:"vin45,omitempty"`
	VirtualInput047 *int `json:"VI47,omitempty" xml:"vin46,omitempty"`
	VirtualInput048 *int `json:"VI48,omitempty" xml:"vin47,omitempty"`
	VirtualInput049 *int `json:"VI49,omitempty" xml:"vin48,omitempty"`
	VirtualInput050 *int `json:"VI50,omitempty" xml:"vin49,omitempty"`
	VirtualInput051 *int `json:"VI51,omitempty" xml:"vin50,omitempty"`
	VirtualInput052 *int `json:"VI52,omitempty" xml:"vin51,omitempty"`
	VirtualInput053 *int `json:"VI53,omitempty" xml:"vin52,omitempty"`
	VirtualInput054 *int `json:"VI54,omitempty" xml:"vin53,omitempty"`
	VirtualInput055 *int `json:"VI55,omitempty" xml:"vin54,omitempty"`
	VirtualInput056 *int `json:"VI56,omitempty" xml:"vin55,omitempty"`
	VirtualInput057 *int `json:"VI57,omitempty" xml:"vin56,omitempty"`
	VirtualInput058 *int `json:"VI58,omitempty" xml:"vin57,omitempty"`
	VirtualInput059 *int `json:"VI59,omitempty" xml:"vin58,omitempty"`
	VirtualInput060 *int `json:"VI60,omitempty" xml:"vin59,omitempty"`
	VirtualInput061 *int `json:"VI61,omitempty" xml:"vin60,omitempty"`
	VirtualInput062 *int `json:"VI62,omitempty" xml:"vin61,omitempty"`
	VirtualInput063 *int `json:"VI63,omitempty" xml:"vin62,omitempty"`
	VirtualInput064 *int `json:"VI64,omitempty" xml:"vin63,omitempty"`
	VirtualInput065 *int `json:"VI65,omitempty" xml:"vin64,omitempty"`
	VirtualInput066 *int `json:"VI66,omitempty" xml:"vin65,omitempty"`
	VirtualInput067 *int `json:"VI67,omitempty" xml:"vin66,omitempty"`
	VirtualInput068 *int `json:"VI68,omitempty" xml:"vin67,omitempty"`
	VirtualInput069 *int `json:"VI69,omitempty" xml:"vin68,omitempty"`
	VirtualInput070 *int `json:"VI70,omitempty" xml:"vin69,omitempty"`
	VirtualInput071 *int `json:"VI71,omitempty" xml:"vin70,omitempty"`
	VirtualInput072 *int `json:"VI72,omitempty" xml:"vin71,omitempty"`
	VirtualInput073 *int `json:"VI73,omitempty" xml:"vin72,omitempty"`
	VirtualInput074 *int `json:"VI74,omitempty" xml:"vin73,omitempty"`
	VirtualInput075 *int `json:"VI75,omitempty" xml:"vin74,omitempty"`
	VirtualInput076 *int `json:"VI76,omitempty" xml:"vin75,omitempty"`
	VirtualInput077 *int `json:"VI77,omitempty" xml:"vin76,omitempty"`
	VirtualInput078 *int `json:"VI78,omitempty" xml:"vin77,omitempty"`
	VirtualInput079 *int `json:"VI79,omitempty" xml:"vin78,omitempty"`
	VirtualInput080 *int `json:"VI80,omitempty" xml:"vin79,omitempty"`
	VirtualInput081 *int `json:"VI81,omitempty" xml:"vin80,omitempty"`
	VirtualInput082 *int `json:"VI82,omitempty" xml:"vin81,omitempty"`
	VirtualInput083 *int `json:"VI83,omitempty" xml:"vin82,omitempty"`
	VirtualInput084 *int `json:"VI84,omitempty" xml:"vin83,omitempty"`
	VirtualInput085 *int `json:"VI85,omitempty" xml:"vin84,omitempty"`
	VirtualInput086 *int `json:"VI86,omitempty" xml:"vin85,omitempty"`
	VirtualInput087 *int `json:"VI87,omitempty" xml:"vin86,omitempty"`
	VirtualInput088 *int `json:"VI88,omitempty" xml:"vin87,omitempty"`
	VirtualInput089 *int `json:"VI89,omitempty" xml:"vin88,omitempty"`
	VirtualInput090 *int `json:"VI90,omitempty" xml:"vin89,omitempty"`
	VirtualInput091 *int `json:"VI91,omitempty" xml:"vin90,omitempty"`
	VirtualInput092 *int `json:"VI92,omitempty" xml:"vin91,omitempty"`
	VirtualInput093 *int `json:"VI93,omitempty" xml:"vin92,omitempty"`
	VirtualInput094 *int `json:"VI94,omitempty" xml:"vin93,omitempty"`
	VirtualInput095 *int `json:"VI95,omitempty" xml:"vin94,omitempty"`
	VirtualInput096 *int `json:"VI96,omitempty" xml:"vin95,omitempty"`
	VirtualInput097 *int `json:"VI97,omitempty" xml:"vin96,omitempty"`
	VirtualInput098 *int `json:"VI98,omitempty" xml:"vin97,omitempty"`
	VirtualInput099 *int `json:"VI99,omitempty" xml:"vin98,omitempty"`
	VirtualInput100 *int `json:"VI100,omitempty" xml:"vin99,omitempty"`
	VirtualInput101 *int `json:"VI101,omitempty" xml:"vin100,omitempty"`
	VirtualInput102 *int `json:"VI102,omitempty" xml:"vin101,omitempty"`
	VirtualInput103 *int `json:"VI103,omitempty" xml:"vin102,omitempty"`
	VirtualInput104 *int `json:"VI104,omitempty" xml:"vin103,omitempty"`
	VirtualInput105 *int `json:"VI105,omitempty" xml:"vin104,omitempty"`
	VirtualInput106 *int `json:"VI106,omitempty" xml:"vin105,omitempty"`
	VirtualInput107 *int `json:"VI107,omitempty" xml:"vin106,omitempty"`
	VirtualInput108 *int `json:"VI108,omitempty" xml:"vin107,omitempty"`
	VirtualInput109 *int `json:"VI109,omitempty" xml:"vin108,omitempty"`
	VirtualInput110 *int `json:"VI110,omitempty" xml:"vin109,omitempty"`
	VirtualInput111 *int `json:"VI111,omitempty" xml:"vin110,omitempty"`
	VirtualInput112 *int `json:"VI112,omitempty" xml:"vin111,omitempty"`
	VirtualInput113 *int `json:"VI113,omitempty" xml:"vin112,omitempty"`
	VirtualInput114 *int `json:"VI114,omitempty" xml:"vin113,omitempty"`
	VirtualInput115 *int `json:"VI115,omitempty" xml:"vin114,omitempty"`
	VirtualInput116 *int `json:"VI116,omitempty" xml:"vin115,omitempty"`
	VirtualInput117 *int `json:"VI117,omitempty" xml:"vin116,omitempty"`
	VirtualInput118 *int `json:"VI118,omitempty" xml:"vin117,omitempty"`
	VirtualInput119 *int `json:"VI119,omitempty" xml:"vin118,omitempty"`
	VirtualInput120 *int `json:"VI120,omitempty" xml:"vin119,omitempty"`
	VirtualInput121 *int `json:"VI121,omitempty" xml:"vin120,omitempty"`
	VirtualInput122 *int `json:"VI122,omitempty" xml:"vin121,omitempty"`
	VirtualInput123 *int `json:"VI123,omitempty" xml:"vin122,omitempty"`
	VirtualInput124 *int `json:"VI124,omitempty" xml:"vin123,omitempty"`
	VirtualInput125 *int `json:"VI125,omitempty" xml:"vin124,omitempty"`
	VirtualInput126 *int `json:"VI126,omitempty" xml:"vin125,omitempty"`
	VirtualInput127 *int `json:"VI127,omitempty" xml:"vin126,omitempty"`
	VirtualInput128 *int `json:"VI128,omitempty" xml:"vin127,omitempty"`

	VirtualAnalog001 *int `json:"VA1,omitempty" xml:"analogV0,omitempty"`
	VirtualAnalog002 *int `json:"VA2,omitempty" xml:"analogV1,omitempty"`
	VirtualAnalog003 *int `json:"VA3,omitempty" xml:"analogV2,omitempty"`
	VirtualAnalog004 *int `json:"VA4,omitempty" xml:"analogV3,omitempty"`
	VirtualAnalog005 *int `json:"VA5,omitempty" xml:"analogV4,omitempty"`
	VirtualAnalog006 *int `json:"VA6,omitempty" xml:"analogV5,omitempty"`
	VirtualAnalog007 *int `json:"VA7,omitempty" xml:"analogV6,omitempty"`
	VirtualAnalog008 *int `json:"VA8,omitempty" xml:"analogV7,omitempty"`
	VirtualAnalog009 *int `json:"VA9,omitempty" xml:"analogV8,omitempty"`
	VirtualAnalog010 *int `json:"VA10,omitempty" xml:"analogV9,omitempty"`
	VirtualAnalog011 *int `json:"VA11,omitempty" xml:"analogV10,omitempty"`
	VirtualAnalog012 *int `json:"VA12,omitempty" xml:"analogV11,omitempty"`
	VirtualAnalog013 *int `json:"VA13,omitempty" xml:"analogV12,omitempty"`
	VirtualAnalog014 *int `json:"VA14,omitempty" xml:"analogV13,omitempty"`
	VirtualAnalog015 *int `json:"VA15,omitempty" xml:"analogV14,omitempty"`
	VirtualAnalog016 *int `json:"VA16,omitempty" xml:"analogV15,omitempty"`
	VirtualAnalog017 *int `json:"VA17,omitempty" xml:"analogV16,omitempty"`
	VirtualAnalog018 *int `json:"VA18,omitempty" xml:"analogV17,omitempty"`
	VirtualAnalog019 *int `json:"VA19,omitempty" xml:"analogV18,omitempty"`
	VirtualAnalog020 *int `json:"VA20,omitempty" xml:"analogV19,omitempty"`
	VirtualAnalog021 *int `json:"VA21,omitempty" xml:"analogV20,omitempty"`
	VirtualAnalog022 *int `json:"VA22,omitempty" xml:"analogV21,omitempty"`
	VirtualAnalog023 *int `json:"VA23,omitempty" xml:"analogV22,omitempty"`
	VirtualAnalog024 *int `json:"VA24,omitempty" xml:"analogV23,omitempty"`
	VirtualAnalog025 *int `json:"VA25,omitempty" xml:"analogV24,omitempty"`
	VirtualAnalog026 *int `json:"VA26,omitempty" xml:"analogV25,omitempty"`
	VirtualAnalog027 *int `json:"VA27,omitempty" xml:"analogV26,omitempty"`
	VirtualAnalog028 *int `json:"VA28,omitempty" xml:"analogV27,omitempty"`
	VirtualAnalog029 *int `json:"VA29,omitempty" xml:"analogV28,omitempty"`
	VirtualAnalog030 *int `json:"VA30,omitempty" xml:"analogV29,omitempty"`
	VirtualAnalog031 *int `json:"VA31,omitempty" xml:"analogV30,omitempty"`
	VirtualAnalog032 *int `json:"VA32,omitempty" xml:"analogV31,omitempty"`

	Thermostat01 *float64 `json:"T1,omitempty" xml:"-"`
	Thermostat02 *float64 `json:"T2,omitempty" xml:"-"`
	Thermostat03 *float64 `json:"T3,omitempty" xml:"-"`
	Thermostat04 *float64 `json:"T4,omitempty" xml:"-"`
	Thermostat05 *float64 `json:"T5,omitempty" xml:"-"`
	Thermostat06 *float64 `json:"T6,omitempty" xml:"-"`
	Thermostat07 *float64 `json:"T7,omitempty" xml:"-"`
	Thermostat08 *float64 `json:"T8,omitempty" xml:"-"`
	Thermostat09 *float64 `json:"T9,omitempty" xml:"-"`
	Thermostat10 *float64 `json:"T10,omitempty" xml:"-"`
	Thermostat11 *float64 `json:"T11,omitempty" xml:"-"`
	Thermostat12 *float64 `json:"T12,omitempty" xml:"-"`
	Thermostat13 *float64 `json:"T13,omitempty" xml:"-"`
	Thermostat14 *float64 `json:"T14,omitempty" xml:"-"`
	Thermostat15 *float64 `json:"T15,omitempty" xml:"-"`
	Thermostat16 *float64 `json:"T16,omitempty" xml:"-"`

	Heater01 interface{} `json:"FP1 Zone 1,omitempty" xml:"x4fp11,omitempty"`
	Heater02 interface{} `json:"FP1 Zone 2,omitempty" xml:"x4fp12,omitempty"`
	Heater03 interface{} `json:"FP1 Zone 3,omitempty" xml:"x4fp13,omitempty"`
	Heater04 interface{} `json:"FP1 Zone 4,omitempty" xml:"x4fp14,omitempty"`
	Heater05 interface{} `json:"FP2 Zone 1,omitempty" xml:"x4fp21,omitempty"`
	Heater06 interface{} `json:"FP2 Zone 2,omitempty" xml:"x4fp22,omitempty"`
	Heater07 interface{} `json:"FP2 Zone 3,omitempty" xml:"x4fp23,omitempty"`
	Heater08 interface{} `json:"FP2 Zone 4,omitempty" xml:"x4fp24,omitempty"`
	Heater09 interface{} `json:"FP3 Zone 1,omitempty" xml:"x4fp31,omitempty"`
	Heater10 interface{} `json:"FP3 Zone 2,omitempty" xml:"x4fp32,omitempty"`
	Heater11 interface{} `json:"FP3 Zone 3,omitempty" xml:"x4fp33,omitempty"`
	Heater12 interface{} `json:"FP3 Zone 4,omitempty" xml:"x4fp34,omitempty"`
	Heater13 interface{} `json:"FP4 Zone 1,omitempty" xml:"x4fp41,omitempty"`
	Heater14 interface{} `json:"FP4 Zone 2,omitempty" xml:"x4fp42,omitempty"`
	Heater15 interface{} `json:"FP4 Zone 3,omitempty" xml:"x4fp43,omitempty"`
	Heater16 interface{} `json:"FP4 Zone 4,omitempty" xml:"x4fp44,omitempty"`
}

func (r Response) Relays() map[int]bool {
	return map[int]bool{
		1:  *r.Relay01 == 1,
		2:  *r.Relay02 == 1,
		3:  *r.Relay03 == 1,
		4:  *r.Relay04 == 1,
		5:  *r.Relay05 == 1,
		6:  *r.Relay06 == 1,
		7:  *r.Relay07 == 1,
		8:  *r.Relay08 == 1,
		9:  *r.Relay09 == 1,
		10: *r.Relay10 == 1,
		11: *r.Relay11 == 1,
		12: *r.Relay12 == 1,
		13: *r.Relay13 == 1,
		14: *r.Relay14 == 1,
		15: *r.Relay15 == 1,
		16: *r.Relay16 == 1,
		17: *r.Relay17 == 1,
		18: *r.Relay18 == 1,
		19: *r.Relay19 == 1,
		20: *r.Relay20 == 1,
		21: *r.Relay21 == 1,
		22: *r.Relay22 == 1,
		23: *r.Relay23 == 1,
		24: *r.Relay24 == 1,
		25: *r.Relay25 == 1,
		26: *r.Relay26 == 1,
		27: *r.Relay27 == 1,
		28: *r.Relay28 == 1,
		29: *r.Relay29 == 1,
		30: *r.Relay30 == 1,
		31: *r.Relay31 == 1,
		32: *r.Relay32 == 1,
		33: *r.Relay33 == 1,
		34: *r.Relay34 == 1,
		35: *r.Relay35 == 1,
		36: *r.Relay36 == 1,
		37: *r.Relay37 == 1,
		38: *r.Relay38 == 1,
		39: *r.Relay39 == 1,
		40: *r.Relay40 == 1,
		41: *r.Relay41 == 1,
		42: *r.Relay42 == 1,
		43: *r.Relay43 == 1,
		44: *r.Relay44 == 1,
		45: *r.Relay45 == 1,
		46: *r.Relay46 == 1,
		47: *r.Relay47 == 1,
		48: *r.Relay48 == 1,
		49: *r.Relay49 == 1,
		50: *r.Relay50 == 1,
		51: *r.Relay51 == 1,
		52: *r.Relay52 == 1,
		53: *r.Relay53 == 1,
		54: *r.Relay54 == 1,
		55: *r.Relay55 == 1,
		56: *r.Relay56 == 1,
	}
}

func (r Response) Digital() map[int]bool {
	return map[int]bool{
		1:  *r.Digital01 == 1,
		2:  *r.Digital02 == 1,
		3:  *r.Digital03 == 1,
		4:  *r.Digital04 == 1,
		5:  *r.Digital05 == 1,
		6:  *r.Digital06 == 1,
		7:  *r.Digital07 == 1,
		8:  *r.Digital08 == 1,
		9:  *r.Digital09 == 1,
		10: *r.Digital10 == 1,
		11: *r.Digital11 == 1,
		12: *r.Digital12 == 1,
		13: *r.Digital13 == 1,
		14: *r.Digital14 == 1,
		15: *r.Digital15 == 1,
		16: *r.Digital16 == 1,
		17: *r.Digital17 == 1,
		18: *r.Digital18 == 1,
		19: *r.Digital19 == 1,
		20: *r.Digital20 == 1,
		21: *r.Digital21 == 1,
		22: *r.Digital22 == 1,
		23: *r.Digital23 == 1,
		24: *r.Digital24 == 1,
		25: *r.Digital25 == 1,
		26: *r.Digital26 == 1,
		27: *r.Digital27 == 1,
		28: *r.Digital28 == 1,
		29: *r.Digital29 == 1,
		30: *r.Digital30 == 1,
		31: *r.Digital31 == 1,
		32: *r.Digital32 == 1,
		33: *r.Digital33 == 1,
		34: *r.Digital34 == 1,
		35: *r.Digital35 == 1,
		36: *r.Digital36 == 1,
		37: *r.Digital37 == 1,
		38: *r.Digital38 == 1,
		39: *r.Digital39 == 1,
		40: *r.Digital40 == 1,
		41: *r.Digital41 == 1,
		42: *r.Digital42 == 1,
		43: *r.Digital43 == 1,
		44: *r.Digital44 == 1,
		45: *r.Digital45 == 1,
		46: *r.Digital46 == 1,
		47: *r.Digital47 == 1,
		48: *r.Digital48 == 1,
		49: *r.Digital49 == 1,
		50: *r.Digital50 == 1,
		51: *r.Digital51 == 1,
		52: *r.Digital52 == 1,
		53: *r.Digital53 == 1,
		54: *r.Digital54 == 1,
		55: *r.Digital55 == 1,
		56: *r.Digital56 == 1,
	}
}

func (r Response) Analog() map[int]int {
	return map[int]int{
		1: *r.Analog1,
		2: *r.Analog2,
		3: *r.Analog3,
		4: *r.Analog4,
	}
}

func (r Response) VirtualOutputs() map[int]bool {
	return map[int]bool{
		1:   *r.VirtualOutput001 == 1,
		2:   *r.VirtualOutput002 == 1,
		3:   *r.VirtualOutput003 == 1,
		4:   *r.VirtualOutput004 == 1,
		5:   *r.VirtualOutput005 == 1,
		6:   *r.VirtualOutput006 == 1,
		7:   *r.VirtualOutput007 == 1,
		8:   *r.VirtualOutput008 == 1,
		9:   *r.VirtualOutput009 == 1,
		10:  *r.VirtualOutput010 == 1,
		11:  *r.VirtualOutput011 == 1,
		12:  *r.VirtualOutput012 == 1,
		13:  *r.VirtualOutput013 == 1,
		14:  *r.VirtualOutput014 == 1,
		15:  *r.VirtualOutput015 == 1,
		16:  *r.VirtualOutput016 == 1,
		17:  *r.VirtualOutput017 == 1,
		18:  *r.VirtualOutput018 == 1,
		19:  *r.VirtualOutput019 == 1,
		20:  *r.VirtualOutput020 == 1,
		21:  *r.VirtualOutput021 == 1,
		22:  *r.VirtualOutput022 == 1,
		23:  *r.VirtualOutput023 == 1,
		24:  *r.VirtualOutput024 == 1,
		25:  *r.VirtualOutput025 == 1,
		26:  *r.VirtualOutput026 == 1,
		27:  *r.VirtualOutput027 == 1,
		28:  *r.VirtualOutput028 == 1,
		29:  *r.VirtualOutput029 == 1,
		30:  *r.VirtualOutput030 == 1,
		31:  *r.VirtualOutput031 == 1,
		32:  *r.VirtualOutput032 == 1,
		33:  *r.VirtualOutput033 == 1,
		34:  *r.VirtualOutput034 == 1,
		35:  *r.VirtualOutput035 == 1,
		36:  *r.VirtualOutput036 == 1,
		37:  *r.VirtualOutput037 == 1,
		38:  *r.VirtualOutput038 == 1,
		39:  *r.VirtualOutput039 == 1,
		40:  *r.VirtualOutput040 == 1,
		41:  *r.VirtualOutput041 == 1,
		42:  *r.VirtualOutput042 == 1,
		43:  *r.VirtualOutput043 == 1,
		44:  *r.VirtualOutput044 == 1,
		45:  *r.VirtualOutput045 == 1,
		46:  *r.VirtualOutput046 == 1,
		47:  *r.VirtualOutput047 == 1,
		48:  *r.VirtualOutput048 == 1,
		49:  *r.VirtualOutput049 == 1,
		50:  *r.VirtualOutput050 == 1,
		51:  *r.VirtualOutput051 == 1,
		52:  *r.VirtualOutput052 == 1,
		53:  *r.VirtualOutput053 == 1,
		54:  *r.VirtualOutput054 == 1,
		55:  *r.VirtualOutput055 == 1,
		56:  *r.VirtualOutput056 == 1,
		57:  *r.VirtualOutput057 == 1,
		58:  *r.VirtualOutput058 == 1,
		59:  *r.VirtualOutput059 == 1,
		60:  *r.VirtualOutput060 == 1,
		61:  *r.VirtualOutput061 == 1,
		62:  *r.VirtualOutput062 == 1,
		63:  *r.VirtualOutput063 == 1,
		64:  *r.VirtualOutput064 == 1,
		65:  *r.VirtualOutput065 == 1,
		66:  *r.VirtualOutput066 == 1,
		67:  *r.VirtualOutput067 == 1,
		68:  *r.VirtualOutput068 == 1,
		69:  *r.VirtualOutput069 == 1,
		70:  *r.VirtualOutput070 == 1,
		71:  *r.VirtualOutput071 == 1,
		72:  *r.VirtualOutput072 == 1,
		73:  *r.VirtualOutput073 == 1,
		74:  *r.VirtualOutput074 == 1,
		75:  *r.VirtualOutput075 == 1,
		76:  *r.VirtualOutput076 == 1,
		77:  *r.VirtualOutput077 == 1,
		78:  *r.VirtualOutput078 == 1,
		79:  *r.VirtualOutput079 == 1,
		80:  *r.VirtualOutput080 == 1,
		81:  *r.VirtualOutput081 == 1,
		82:  *r.VirtualOutput082 == 1,
		83:  *r.VirtualOutput083 == 1,
		84:  *r.VirtualOutput084 == 1,
		85:  *r.VirtualOutput085 == 1,
		86:  *r.VirtualOutput086 == 1,
		87:  *r.VirtualOutput087 == 1,
		88:  *r.VirtualOutput088 == 1,
		89:  *r.VirtualOutput089 == 1,
		90:  *r.VirtualOutput090 == 1,
		91:  *r.VirtualOutput091 == 1,
		92:  *r.VirtualOutput092 == 1,
		93:  *r.VirtualOutput093 == 1,
		94:  *r.VirtualOutput094 == 1,
		95:  *r.VirtualOutput095 == 1,
		96:  *r.VirtualOutput096 == 1,
		97:  *r.VirtualOutput097 == 1,
		98:  *r.VirtualOutput098 == 1,
		99:  *r.VirtualOutput099 == 1,
		100: *r.VirtualOutput100 == 1,
		101: *r.VirtualOutput101 == 1,
		102: *r.VirtualOutput102 == 1,
		103: *r.VirtualOutput103 == 1,
		104: *r.VirtualOutput104 == 1,
		105: *r.VirtualOutput105 == 1,
		106: *r.VirtualOutput106 == 1,
		107: *r.VirtualOutput107 == 1,
		108: *r.VirtualOutput108 == 1,
		109: *r.VirtualOutput109 == 1,
		110: *r.VirtualOutput110 == 1,
		111: *r.VirtualOutput111 == 1,
		112: *r.VirtualOutput112 == 1,
		113: *r.VirtualOutput113 == 1,
		114: *r.VirtualOutput114 == 1,
		115: *r.VirtualOutput115 == 1,
		116: *r.VirtualOutput116 == 1,
		117: *r.VirtualOutput117 == 1,
		118: *r.VirtualOutput118 == 1,
		119: *r.VirtualOutput119 == 1,
		120: *r.VirtualOutput120 == 1,
		121: *r.VirtualOutput121 == 1,
		122: *r.VirtualOutput122 == 1,
		123: *r.VirtualOutput123 == 1,
		124: *r.VirtualOutput124 == 1,
		125: *r.VirtualOutput125 == 1,
		126: *r.VirtualOutput126 == 1,
		127: *r.VirtualOutput127 == 1,
		128: *r.VirtualOutput128 == 1,
	}
}

func (r Response) VirtualInputs() map[int]bool {
	return map[int]bool{
		1:   *r.VirtualInput001 == 1,
		2:   *r.VirtualInput002 == 1,
		3:   *r.VirtualInput003 == 1,
		4:   *r.VirtualInput004 == 1,
		5:   *r.VirtualInput005 == 1,
		6:   *r.VirtualInput006 == 1,
		7:   *r.VirtualInput007 == 1,
		8:   *r.VirtualInput008 == 1,
		9:   *r.VirtualInput009 == 1,
		10:  *r.VirtualInput010 == 1,
		11:  *r.VirtualInput011 == 1,
		12:  *r.VirtualInput012 == 1,
		13:  *r.VirtualInput013 == 1,
		14:  *r.VirtualInput014 == 1,
		15:  *r.VirtualInput015 == 1,
		16:  *r.VirtualInput016 == 1,
		17:  *r.VirtualInput017 == 1,
		18:  *r.VirtualInput018 == 1,
		19:  *r.VirtualInput019 == 1,
		20:  *r.VirtualInput020 == 1,
		21:  *r.VirtualInput021 == 1,
		22:  *r.VirtualInput022 == 1,
		23:  *r.VirtualInput023 == 1,
		24:  *r.VirtualInput024 == 1,
		25:  *r.VirtualInput025 == 1,
		26:  *r.VirtualInput026 == 1,
		27:  *r.VirtualInput027 == 1,
		28:  *r.VirtualInput028 == 1,
		29:  *r.VirtualInput029 == 1,
		30:  *r.VirtualInput030 == 1,
		31:  *r.VirtualInput031 == 1,
		32:  *r.VirtualInput032 == 1,
		33:  *r.VirtualInput033 == 1,
		34:  *r.VirtualInput034 == 1,
		35:  *r.VirtualInput035 == 1,
		36:  *r.VirtualInput036 == 1,
		37:  *r.VirtualInput037 == 1,
		38:  *r.VirtualInput038 == 1,
		39:  *r.VirtualInput039 == 1,
		40:  *r.VirtualInput040 == 1,
		41:  *r.VirtualInput041 == 1,
		42:  *r.VirtualInput042 == 1,
		43:  *r.VirtualInput043 == 1,
		44:  *r.VirtualInput044 == 1,
		45:  *r.VirtualInput045 == 1,
		46:  *r.VirtualInput046 == 1,
		47:  *r.VirtualInput047 == 1,
		48:  *r.VirtualInput048 == 1,
		49:  *r.VirtualInput049 == 1,
		50:  *r.VirtualInput050 == 1,
		51:  *r.VirtualInput051 == 1,
		52:  *r.VirtualInput052 == 1,
		53:  *r.VirtualInput053 == 1,
		54:  *r.VirtualInput054 == 1,
		55:  *r.VirtualInput055 == 1,
		56:  *r.VirtualInput056 == 1,
		57:  *r.VirtualInput057 == 1,
		58:  *r.VirtualInput058 == 1,
		59:  *r.VirtualInput059 == 1,
		60:  *r.VirtualInput060 == 1,
		61:  *r.VirtualInput061 == 1,
		62:  *r.VirtualInput062 == 1,
		63:  *r.VirtualInput063 == 1,
		64:  *r.VirtualInput064 == 1,
		65:  *r.VirtualInput065 == 1,
		66:  *r.VirtualInput066 == 1,
		67:  *r.VirtualInput067 == 1,
		68:  *r.VirtualInput068 == 1,
		69:  *r.VirtualInput069 == 1,
		70:  *r.VirtualInput070 == 1,
		71:  *r.VirtualInput071 == 1,
		72:  *r.VirtualInput072 == 1,
		73:  *r.VirtualInput073 == 1,
		74:  *r.VirtualInput074 == 1,
		75:  *r.VirtualInput075 == 1,
		76:  *r.VirtualInput076 == 1,
		77:  *r.VirtualInput077 == 1,
		78:  *r.VirtualInput078 == 1,
		79:  *r.VirtualInput079 == 1,
		80:  *r.VirtualInput080 == 1,
		81:  *r.VirtualInput081 == 1,
		82:  *r.VirtualInput082 == 1,
		83:  *r.VirtualInput083 == 1,
		84:  *r.VirtualInput084 == 1,
		85:  *r.VirtualInput085 == 1,
		86:  *r.VirtualInput086 == 1,
		87:  *r.VirtualInput087 == 1,
		88:  *r.VirtualInput088 == 1,
		89:  *r.VirtualInput089 == 1,
		90:  *r.VirtualInput090 == 1,
		91:  *r.VirtualInput091 == 1,
		92:  *r.VirtualInput092 == 1,
		93:  *r.VirtualInput093 == 1,
		94:  *r.VirtualInput094 == 1,
		95:  *r.VirtualInput095 == 1,
		96:  *r.VirtualInput096 == 1,
		97:  *r.VirtualInput097 == 1,
		98:  *r.VirtualInput098 == 1,
		99:  *r.VirtualInput099 == 1,
		100: *r.VirtualInput100 == 1,
		101: *r.VirtualInput101 == 1,
		102: *r.VirtualInput102 == 1,
		103: *r.VirtualInput103 == 1,
		104: *r.VirtualInput104 == 1,
		105: *r.VirtualInput105 == 1,
		106: *r.VirtualInput106 == 1,
		107: *r.VirtualInput107 == 1,
		108: *r.VirtualInput108 == 1,
		109: *r.VirtualInput109 == 1,
		110: *r.VirtualInput110 == 1,
		111: *r.VirtualInput111 == 1,
		112: *r.VirtualInput112 == 1,
		113: *r.VirtualInput113 == 1,
		114: *r.VirtualInput114 == 1,
		115: *r.VirtualInput115 == 1,
		116: *r.VirtualInput116 == 1,
		117: *r.VirtualInput117 == 1,
		118: *r.VirtualInput118 == 1,
		119: *r.VirtualInput119 == 1,
		120: *r.VirtualInput120 == 1,
		121: *r.VirtualInput121 == 1,
		122: *r.VirtualInput122 == 1,
		123: *r.VirtualInput123 == 1,
		124: *r.VirtualInput124 == 1,
		125: *r.VirtualInput125 == 1,
		126: *r.VirtualInput126 == 1,
		127: *r.VirtualInput127 == 1,
		128: *r.VirtualInput128 == 1,
	}
}

func (r Response) VirtualAnalogs() map[int]int {
	return map[int]int{
		1:  *r.VirtualAnalog001,
		2:  *r.VirtualAnalog002,
		3:  *r.VirtualAnalog003,
		4:  *r.VirtualAnalog004,
		5:  *r.VirtualAnalog005,
		6:  *r.VirtualAnalog006,
		7:  *r.VirtualAnalog007,
		8:  *r.VirtualAnalog008,
		9:  *r.VirtualAnalog009,
		10: *r.VirtualAnalog010,
		11: *r.VirtualAnalog011,
		12: *r.VirtualAnalog012,
		13: *r.VirtualAnalog013,
		14: *r.VirtualAnalog014,
		15: *r.VirtualAnalog015,
		16: *r.VirtualAnalog016,
		17: *r.VirtualAnalog017,
		18: *r.VirtualAnalog018,
		19: *r.VirtualAnalog019,
		20: *r.VirtualAnalog020,
		21: *r.VirtualAnalog021,
		22: *r.VirtualAnalog022,
		23: *r.VirtualAnalog023,
		24: *r.VirtualAnalog024,
		25: *r.VirtualAnalog025,
		26: *r.VirtualAnalog026,
		27: *r.VirtualAnalog027,
		28: *r.VirtualAnalog028,
		29: *r.VirtualAnalog029,
		30: *r.VirtualAnalog030,
		31: *r.VirtualAnalog031,
		32: *r.VirtualAnalog032,
	}
}

func (r Response) Thermostats() map[int]float64 {
	return map[int]float64{
		1:  *r.Thermostat01,
		2:  *r.Thermostat02,
		3:  *r.Thermostat03,
		4:  *r.Thermostat04,
		5:  *r.Thermostat05,
		6:  *r.Thermostat06,
		7:  *r.Thermostat07,
		8:  *r.Thermostat08,
		9:  *r.Thermostat09,
		10: *r.Thermostat10,
		11: *r.Thermostat11,
		12: *r.Thermostat12,
		13: *r.Thermostat13,
		14: *r.Thermostat14,
		15: *r.Thermostat15,
		16: *r.Thermostat16,
	}
}

func (r Response) Heaters() map[int]int {
	return map[int]int{
		1:  ParseHeaterState(r.Heater01).ToInt(),
		2:  ParseHeaterState(r.Heater02).ToInt(),
		3:  ParseHeaterState(r.Heater03).ToInt(),
		4:  ParseHeaterState(r.Heater04).ToInt(),
		5:  ParseHeaterState(r.Heater05).ToInt(),
		6:  ParseHeaterState(r.Heater06).ToInt(),
		7:  ParseHeaterState(r.Heater07).ToInt(),
		8:  ParseHeaterState(r.Heater08).ToInt(),
		9:  ParseHeaterState(r.Heater09).ToInt(),
		10: ParseHeaterState(r.Heater10).ToInt(),
		11: ParseHeaterState(r.Heater11).ToInt(),
		12: ParseHeaterState(r.Heater12).ToInt(),
		13: ParseHeaterState(r.Heater13).ToInt(),
		14: ParseHeaterState(r.Heater14).ToInt(),
		15: ParseHeaterState(r.Heater15).ToInt(),
		16: ParseHeaterState(r.Heater16).ToInt(),
	}
}
