package ipx

import (
	"fmt"
	"strings"
)

type HeaterState int

const (
	HeaterStateUnknown   HeaterState = -1
	HeaterStateComfort   HeaterState = 0
	HeaterStateEco       HeaterState = 1
	HeaterStateFrostFree HeaterState = 2
	HeaterStateHalt      HeaterState = 3
	HeaterStateComfort1  HeaterState = 4
	HeaterStateComfort2  HeaterState = 5
)

func (s HeaterState) ToInt() int {
	return int(s)
}

func ParseHeaterState(v interface{}) HeaterState {
	switch strings.ToLower(fmt.Sprintf("%v", v)) {
	case "confort":
		return HeaterStateComfort
	case "eco":
		return HeaterStateEco
	case "hors gel":
		return HeaterStateFrostFree
	case "arret":
		return HeaterStateHalt
	case "confort -1":
		return HeaterStateComfort1
	case "confort -2":
		return HeaterStateComfort2
	default:
		return HeaterStateUnknown
	}
}

type Heater interface {
	ID() int
	Name() string
	Value() HeaterState
	Set(state HeaterState) error
}

type heater struct {
	id    int
	value HeaterState
	api   API
}

func (h heater) ID() int {
	return h.id
}

func (h heater) Name() string {
	return fmt.Sprintf("FP%0.2d", h.id)
}

func (h heater) Value() HeaterState {
	return h.value
}

func (h heater) Set(state HeaterState) error {
	_, err := h.api.GET(map[string]interface{}{fmt.Sprintf("SetFP%0.2d", h.id): state})
	if err != nil {
		return err
	}

	return nil
}
