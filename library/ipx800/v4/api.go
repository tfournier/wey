package ipx

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	refresh = time.Second * 5
	timeout = time.Second * 2
	jsonURI = "/api/xdevices.json"
	xmlURI  = "/user/status.xml"
)

var (
	ErrInvalidRequest     = errors.New("invalid request")
	ErrInvalidContentType = errors.New("invalid content-type")
	ErrClientResponse     = errors.New("worker error response")
)

type API interface {
	GET(commands map[string]interface{}) (*Response, error)
}

type Client struct {
	Address        string
	ApiKey         interface{}
	MacAddress     string
	Relays         map[int]Relay
	Digital        map[int]Digital
	Analog         map[int]Analog
	VirtualOutputs map[int]VirtualOutput
	VirtualInputs  map[int]VirtualInput
	VirtualAnalogs map[int]VirtualAnalog
	Thermostats    map[int]Thermostat
	Heater         map[int]Heater
}

func New(address string, apiKey interface{}) (*Client, error) {
	c := &Client{
		Address:        address,
		ApiKey:         apiKey,
		Relays:         make(map[int]Relay),
		Digital:        make(map[int]Digital),
		Analog:         make(map[int]Analog),
		VirtualOutputs: make(map[int]VirtualOutput),
		VirtualInputs:  make(map[int]VirtualInput),
		VirtualAnalogs: make(map[int]VirtualAnalog),
		Thermostats:    make(map[int]Thermostat),
		Heater:         make(map[int]Heater),
	}

	status, err := c.request(http.MethodGet, xmlURI, nil)
	if err != nil {
		return nil, err
	}

	if status.MacAddress != nil {
		c.MacAddress = *status.MacAddress
	}

	if err := c.load(); err != nil {
		return nil, err
	}

	go func() {
		time.Sleep(refresh)
		for {
			if err := c.load(); err != nil {
				log.Println(err)
			}
			time.Sleep(refresh)
		}
	}()

	return c, nil
}

func (c *Client) load() error {
	res, err := c.GET(map[string]interface{}{"Get": "all"})
	if err != nil {
		return err
	}

	c.parseRelays(res)
	c.parseDigital(res)
	c.parseAnalog(res)
	c.parseVirtualOutputs(res)
	c.parseVirtualInputs(res)
	c.parseVirtualAnalogs(res)
	c.parseThermostats(res)
	c.parseHeater(res)

	return nil
}

func (c Client) url(uri string, commands map[string]interface{}) string {
	baseURL := fmt.Sprintf("http://%s%s", c.Address, uri)

	var opts []string

	if c.ApiKey != nil {
		opts = append(opts, fmt.Sprintf("key=%v", c.ApiKey))
	}

	if commands != nil {
		for k, v := range commands {
			opts = append(opts, fmt.Sprintf("%s=%v", k, v))
		}
	}

	return fmt.Sprintf("%s?%s", baseURL, strings.Join(opts, "&"))
}

func (c Client) request(method string, uri string, commands map[string]interface{}) (*Response, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), timeout)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, method, c.url(uri, commands), nil)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	ret, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	defer ret.Body.Close()

	if http.StatusBadRequest <= ret.StatusCode {
		return nil, ErrInvalidRequest
	}

	body, err := ioutil.ReadAll(ret.Body)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	res := &Response{}

	var unmarshalError error
	switch ret.Header.Get("Content-Type") {
	case "application/json":
		unmarshalError = json.Unmarshal(body, res)
	case "text/xml":
		unmarshalError = xml.Unmarshal(body, &res)
	default:
		unmarshalError = ErrInvalidContentType
	}

	if unmarshalError != nil {
		return nil, errors.Unwrap(err)
	}

	if res.Status != nil {
		if strings.ToLower(*res.Status) == "error" {
			return nil, ErrClientResponse
		}
	}

	return res, nil
}

func (c Client) GET(commands map[string]interface{}) (*Response, error) {
	return c.request(http.MethodGet, jsonURI, commands)
}

func (c *Client) parseRelays(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.Relays() {
		c.Relays[k] = &relay{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseDigital(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.Digital() {
		c.Digital[k] = &digital{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseAnalog(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.Analog() {
		c.Analog[k] = &analog{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseVirtualOutputs(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.VirtualOutputs() {
		c.VirtualOutputs[k] = &virtualOutput{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseVirtualInputs(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.VirtualInputs() {
		c.VirtualInputs[k] = &virtualInput{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseVirtualAnalogs(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.VirtualAnalogs() {
		c.VirtualAnalogs[k] = &virtualAnalog{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseThermostats(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.Thermostats() {
		c.Thermostats[k] = &thermostat{
			api:   c,
			id:    k,
			value: v,
		}
	}
}

func (c *Client) parseHeater(res *Response) {
	if res == nil {
		return
	}

	for k, v := range res.Heaters() {
		c.Heater[k] = &heater{
			api:   c,
			id:    k,
			value: HeaterState(v),
		}
	}
}
