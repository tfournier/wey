package ipx

import "fmt"

type Thermostat interface {
	ID() int
	Name() string
	Value() float64
	Set(options ThermostatOptions) error
}

type ThermostatOptions struct {
	Consign     *float64
	ConsignHigh *float64
	ConsignLow  *float64
	Hysteresis  *int
}

type thermostat struct {
	id    int
	value float64
	api   API
}

func (t thermostat) ID() int {
	return t.id
}

func (t thermostat) Name() string {
	return fmt.Sprintf("T%0.2d", t.id)
}

func (t thermostat) Value() float64 {
	return t.value
}

func (t thermostat) Set(options ThermostatOptions) error {
	cmd := map[string]interface{}{
		"SetThermo": t.id,
	}

	if options.Consign != nil {
		cmd["Cons"] = *options.Consign
	}

	if options.ConsignHigh != nil {
		cmd["ConsPlus"] = *options.ConsignHigh
	}

	if options.ConsignLow != nil {
		cmd["ConsMoins"] = *options.ConsignLow
	}

	if options.Hysteresis != nil {
		cmd["Hys"] = *options.Hysteresis
	}

	_, err := t.api.GET(cmd)
	if err != nil {
		return err
	}

	return nil
}
