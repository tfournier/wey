package ipx

import "fmt"

type Analog interface {
	ID() int
	Name() string
	Value() int
}

type analog struct {
	id    int
	value int
	api   API
}

func (a analog) ID() int {
	return a.id
}

func (a analog) Name() string {
	return fmt.Sprintf("A%0.1d", a.id)
}

func (a analog) Value() int {
	return a.value
}
