package ipx

import "fmt"

type VirtualOutput interface {
	ID() int
	Name() string
	Value() bool
	Set() error
	Clear() error
	Toggle() error
}

type virtualOutput struct {
	id    int
	value bool
	api   API
}

func (o virtualOutput) ID() int {
	return o.id
}

func (o virtualOutput) Name() string {
	return fmt.Sprintf("VI%0.3d", o.id)
}

func (o virtualOutput) Value() bool {
	return o.value
}

func (o virtualOutput) Set() error {
	_, err := o.api.GET(map[string]interface{}{"SetVO": fmt.Sprintf("%0.3d", o.id)})
	if err != nil {
		return err
	}

	return nil
}

func (o virtualOutput) Clear() error {
	_, err := o.api.GET(map[string]interface{}{"ClearVO": fmt.Sprintf("%0.3d", o.id)})
	if err != nil {
		return err
	}

	return nil
}

func (o virtualOutput) Toggle() error {
	_, err := o.api.GET(map[string]interface{}{"ToggleVO": fmt.Sprintf("%0.3d", o.id)})
	if err != nil {
		return err
	}

	return nil
}
