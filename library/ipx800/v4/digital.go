package ipx

import "fmt"

type Digital interface {
	ID() int
	Name() string
	Value() bool
}

type digital struct {
	id    int
	value bool
	api   API
}

func (d digital) ID() int {
	return d.id
}

func (d digital) Name() string {
	return fmt.Sprintf("D%0.2d", d.id)
}

func (d digital) Value() bool {
	return d.value
}
