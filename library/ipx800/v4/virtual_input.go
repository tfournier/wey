package ipx

import "fmt"

type VirtualInput interface {
	ID() int
	Name() string
	Value() bool
	Set() error
	Clear() error
	Toggle() error
}

type virtualInput struct {
	id    int
	value bool
	api   API
}

func (i virtualInput) ID() int {
	return i.id
}

func (i virtualInput) Name() string {
	return fmt.Sprintf("VI%0.3d", i.id)
}

func (i virtualInput) Value() bool {
	return i.value
}

func (i virtualInput) Set() error {
	_, err := i.api.GET(map[string]interface{}{"SetVI": fmt.Sprintf("%0.3d", i.id)})
	if err != nil {
		return err
	}

	return nil
}

func (i virtualInput) Clear() error {
	_, err := i.api.GET(map[string]interface{}{"ClearVI": fmt.Sprintf("%0.3d", i.id)})
	if err != nil {
		return err
	}

	return nil
}

func (i virtualInput) Toggle() error {
	_, err := i.api.GET(map[string]interface{}{"ToggleVI": fmt.Sprintf("%0.3d", i.id)})
	if err != nil {
		return err
	}

	return nil
}
