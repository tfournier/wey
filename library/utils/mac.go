package utils

import (
	"bytes"
	"net"
)

func MacAddress() string {
	interfaces, err := net.Interfaces()
	if err != nil {
		return ""
	}

	for _, i := range interfaces {
		if i.Flags&net.FlagUp != 0 && !bytes.Equal(i.HardwareAddr, nil) {
			if i.HardwareAddr[0]&2 == 2 {
				continue
			}

			return i.HardwareAddr.String()
		}
	}

	return ""
}
