package internal

import (
	"github.com/brutella/hc/accessory"

	ipx "gitlab.com/tfournier/wey/library/ipx800/v4"
)

type Accessories struct {
	Output  []Output  `json:"output" yaml:"output"`
	Digital []Digital `json:"digital" yaml:"digital"`
}

func (a Accessories) List(api *ipx.Client) []*accessory.Accessory {
	var as []*accessory.Accessory

	for _, r := range a.Output {
		if acc := r.Accessory(api); acc != nil {
			as = append(as, acc)
		}
	}

	for _, r := range a.Digital {
		if acc := r.Accessory(api); acc != nil {
			as = append(as, acc)
		}
	}

	return as
}
