package internal

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/util"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger"
	"gitlab.com/tfournier/wey/internal/docs"
	"gitlab.com/tfournier/wey/library/homekit"

	rice "github.com/GeertJohan/go.rice"
	logger "github.com/brutella/hc/log"
)

type ui struct {
	e *echo.Echo
}

// @title Wey
// @description Wey is a GCE Electronics IPX800 (version 4) gateway to control your home.

// @contact.name Thomas FOURNIER
// @contact.url https://gitlab.com/tfournier/wey/-/issues/new
// @contact.email tfournier59@gmail.com

// @license.name MIT
// @license.url https://gitlab.com/tfournier/wey/-/blob/master/LICENSE

// @BasePath /api
func newUI(gw Gateway) ui {
	e := echo.New()

	// Hide default
	e.HideBanner = true
	e.HidePort = true

	// Middleware
	e.Use(middleware.Recover())
	//e.Use(middleware.Logger())
	e.Use(setCustomContext(gw))

	// Embed resources
	assetHandler := http.FileServer(rice.MustFindBox("ui").HTTPBox())

	// Swagger
	docs.SwaggerInfo.Version = version

	// Routes
	e.GET("/", echo.WrapHandler(assetHandler))
	e.GET("/static/*", echo.WrapHandler(assetHandler))
	e.GET("/docs/*", echoSwagger.WrapHandler)
	e.GET("/api/info", api{}.InfoGet)
	e.GET("/api/config", api{}.ConfigGet)
	e.PATCH("/api/config", api{}.ConfigPatch)
	e.GET("/api/svg", api{}.StickerGet)

	return ui{e: e}
}

func (ui ui) Start() {
	logger.Info.Println("WebUI starting on port 80 ...")

	_ = ui.e.Start("0.0.0.0:80")
}

func (ui ui) Stop() {
	logger.Info.Println("WebUI shutdown ...")

	_ = ui.e.Shutdown(context.Background())
}

type customContext struct {
	echo.Context
	gw Gateway
}

func setCustomContext(gw Gateway) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &customContext{
				Context: c,
				gw:      gw,
			}

			return next(cc)
		}
	}
}

type api struct{}

// ConfigGet godoc
// @Summary Get config
// @Accept  json
// @Produce  json
// @Success 200 {object} Config
// @Failure 500 {object} v4.HTTPError
// @Router /config [get]
func (api) ConfigGet(c echo.Context) error {
	cfg := NewConfig()

	if err := cfg.Load(); err != nil {
		return err
	}

	return c.JSONPretty(http.StatusOK, cfg, "  ")
}

// ConfigPatch godoc
// @Summary Update config
// @Accept  json
// @Produce  json
// @Success 204
// @Failure 500 {object} v4.HTTPError
// @Router /config [patch]
func (api) ConfigPatch(c echo.Context) error {
	defer c.Request().Body.Close()

	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return errors.Unwrap(err)
	}

	cfg := &Config{}

	if err := json.Unmarshal(body, cfg); err != nil {
		return errors.Unwrap(err)
	}

	if err := cfg.Write(); err != nil {
		return err
	}

	go func() {
		time.Sleep(time.Second)

		if p, err := os.FindProcess(os.Getpid()); err == nil {
			_ = p.Signal(os.Interrupt)
		}
	}()

	return c.NoContent(http.StatusNoContent)
}

// StickerGet godoc
// @Summary Generate HomeKit sticker
// @Accept  json
// @Produce image/svg+xml
// @Success 200
// @Router /svg [get]
func (api) StickerGet(c echo.Context) error {
	cc := c.(*customContext)

	sticker, err := homekit.NewQRCode(cc.gw.config.Pin, cc.gw.config.SetupId, accessory.TypeBridge, util.SetupFlagIP)
	if err != nil {
		return err
	}

	return cc.Stream(http.StatusOK, "image/svg+xml", bytes.NewReader(sticker))
	//return cc.Blob(http.StatusOK, "image/svg+xml", sticker)
}

// ConfigGet godoc
// @Summary Get information
// @Accept  json
// @Produce  json
// @Success 200 {array} string
// @Failure 500 {object} v4.HTTPError
// @Router /info [get]
func (api) InfoGet(c echo.Context) error {
	cc := c.(*customContext)

	res := map[string]string{
		"Manufacturer": cc.gw.bridge.Manufacturer,
		"Model":        cc.gw.bridge.Model,
		"Name":         cc.gw.bridge.Name,
		"Serial":       cc.gw.bridge.SerialNumber,
		"Version":      cc.gw.bridge.FirmwareRevision,
	}

	return c.JSONPretty(http.StatusOK, res, "  ")
}
