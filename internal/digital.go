package internal

import (
	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/characteristic"
	"github.com/brutella/hc/service"
	ipx "gitlab.com/tfournier/wey/library/ipx800/v4"
	"strings"
	"time"
)

type Digital struct {
	ID      int    `json:"id" yaml:"id"`
	Name    string `json:"name" yaml:"name"`
	Type    string `json:"type" yaml:"type"`
	Virtual bool   `json:"virtual" yaml:"virtual"`
}

func (d Digital) IsValid() bool {
	if d.Virtual {
		return 1 <= d.ID && d.ID <= 128
	}

	return 1 <= d.ID && d.ID <= 56
}

func (d Digital) Accessory(api *ipx.Client) *accessory.Accessory {
	if !d.IsValid() {
		return nil
	}

	info := accessory.Info{
		Name:         d.Name,
		SerialNumber: api.MacAddress,
		Manufacturer: "GCE ELECTRONICS",
		Model:        "IPX-800 - V4",
	}

	switch strings.ToLower(d.Type) {
	case "switch":
		return d.switchAccessory(info, api)
	case "leak":
		return d.leakSensorAccessory(info, api)
	default:
		return nil
	}
}

func (d Digital) switchAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	if d.Virtual {
		return d.virtualSwitchAccessory(info, api)
	}

	return d.physicalSwitchAccessory(info, api)
}

func (d Digital) physicalSwitchAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	acc := accessory.NewSwitch(info)

	acc.Switch.On.OnValueRemoteGet(func() bool {
		return api.Digital[d.ID].Value()
	})

	return acc.Accessory
}

func (d Digital) virtualSwitchAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	acc := accessory.NewSwitch(info)

	acc.Switch.On.OnValueRemoteGet(func() bool {
		return api.VirtualInputs[d.ID].Value()
	})

	acc.Switch.On.OnValueRemoteUpdate(func(b bool) {
		if b {
			_ = api.VirtualInputs[d.ID].Set()
		} else {
			_ = api.VirtualInputs[d.ID].Clear()
		}
	})

	return acc.Accessory
}

func (d Digital) leakSensorAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	if d.Virtual {
		return d.virtualLeakSensorAccessory(info, api)
	}

	return d.physicalLeakSensorAccessory(info, api)
}

func (d Digital) physicalLeakSensorAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	svc := service.NewLeakSensor()

	go func() {
		leak := api.Digital[d.ID].Value()

		for {
			time.Sleep(time.Second)

			if state := api.Digital[d.ID].Value(); leak != state {
				v := characteristic.LeakDetectedLeakNotDetected

				if state {
					v = characteristic.LeakDetectedLeakDetected
				}

				svc.LeakDetected.SetValue(v)
				leak = state
			}
		}
	}()

	svc.LeakDetected.OnValueRemoteGet(func() int {
		if api.Digital[d.ID].Value() {
			return characteristic.LeakDetectedLeakDetected
		}

		return characteristic.LeakDetectedLeakNotDetected
	})

	acc := accessory.New(info, accessory.TypeSensor)
	acc.AddService(svc.Service)

	return acc
}

func (d Digital) virtualLeakSensorAccessory(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	svc := service.NewLeakSensor()

	go func() {
		leak := api.VirtualInputs[d.ID].Value()

		for {
			time.Sleep(time.Second)

			if state := api.VirtualInputs[d.ID].Value(); leak != state {
				v := characteristic.LeakDetectedLeakNotDetected

				if state {
					v = characteristic.LeakDetectedLeakDetected
				}

				svc.LeakDetected.SetValue(v)
				leak = state
			}
		}
	}()

	svc.LeakDetected.OnValueRemoteGet(func() int {
		if api.VirtualInputs[d.ID].Value() {
			return characteristic.LeakDetectedLeakDetected
		}

		return characteristic.LeakDetectedLeakNotDetected
	})

	acc := accessory.New(info, accessory.TypeSensor)
	acc.AddService(svc.Service)

	return acc
}
