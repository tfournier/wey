package internal

import (
	"fmt"

	"github.com/brutella/hc/accessory"

	ipx "gitlab.com/tfournier/wey/library/ipx800/v4"
)

const relaysLengthPerExtension = 8

type Output struct {
	ID      int    `json:"id" yaml:"id"`
	Name    string `json:"name" yaml:"name"`
	Virtual bool   `json:"virtual" yaml:"virtual"`
}

func (o Output) IsValid() bool {
	if o.Virtual {
		return 1 <= o.ID && o.ID <= 128
	}

	return 1 <= o.ID && o.ID <= 56
}

func (o Output) Accessory(api *ipx.Client) *accessory.Accessory {
	if !o.IsValid() {
		return nil
	}

	info := accessory.Info{
		Name:         o.Name,
		SerialNumber: api.MacAddress,
		Manufacturer: "GCE ELECTRONICS",
		Model:        "IPX-800 - V4",
	}

	if !o.Virtual {
		if i := (o.ID - 1) / relaysLengthPerExtension; i > 0 {
			info.Model = fmt.Sprintf("X-8R - %d", i)
		}
	}

	if o.Virtual {
		return o.virtualOutput(info, api)
	}

	return o.physicalOutput(info, api)
}

func (o Output) physicalOutput(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	acc := accessory.NewOutlet(info)

	acc.Outlet.On.OnValueRemoteGet(func() bool {
		return api.Relays[o.ID].Value()
	})

	acc.Outlet.On.OnValueRemoteUpdate(func(b bool) {
		if b {
			_ = api.Relays[o.ID].Set()
		} else {
			_ = api.Relays[o.ID].Clear()
		}
	})

	return acc.Accessory
}

func (o Output) virtualOutput(info accessory.Info, api *ipx.Client) *accessory.Accessory {
	acc := accessory.NewOutlet(info)

	acc.Outlet.On.OnValueRemoteGet(func() bool {
		return api.VirtualOutputs[o.ID].Value()
	})

	acc.Outlet.On.OnValueRemoteUpdate(func(b bool) {
		if b {
			_ = api.VirtualOutputs[o.ID].Set()
		} else {
			_ = api.VirtualOutputs[o.ID].Clear()
		}
	})

	return acc.Accessory
}
