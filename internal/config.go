package internal

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"

	"github.com/jinzhu/configor"
)

var ErrConfigInvalidExtension = errors.New("invalid config extension")

func NewConfig() *Config {
	return &Config{
		Address: "",
		APIKey:  nil,
		Accessories: Accessories{
			Output: []Output{},
		},
	}
}

type Config struct {
	Address     string      `json:"address" yaml:"address"`
	APIKey      interface{} `json:"api_key" yaml:"api_key"`
	Accessories Accessories `json:"accessories" yaml:"accessories"`
}

func (c *Config) Load() error {
	c.Accessories = Accessories{
		Output: []Output{},
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		if err := c.Write(); err != nil {
			return err
		}
	}

	return configor.New(&configor.Config{Silent: true}).Load(c, configPath)
}

func (c Config) Write() error {
	var (
		res []byte
		err error
	)

	switch ext := filepath.Ext(configPath); ext {
	case ".json":
		res, err = json.MarshalIndent(c, "", "  ")
	case ".yml", ".yaml":
		res, err = yaml.Marshal(c)
	default:
		res, err = nil, ErrConfigInvalidExtension
	}

	if err != nil {
		return err
	}

	return ioutil.WriteFile(configPath, res, 0o600)
}
