package internal

import (
	"errors"
	"fmt"
	"log"
	"runtime"
	"strings"

	"github.com/brutella/hc"
	"github.com/brutella/hc/accessory"
	"gitlab.com/tfournier/wey/library/utils"

	logger "github.com/brutella/hc/log"
	ipx "gitlab.com/tfournier/wey/library/ipx800/v4"
)

const (
	configPath = "./config.json"
)

var (
	version = "v0.0.0"
	commit  = "none"
	date    = "unknown"
)

type Gateway struct {
	cfg    *Config
	t      hc.Transport
	config hc.Config
	bridge accessory.Info
	onlyUI bool
}

func New(debug bool) (*Gateway, error) {
	logger.Info.SetFlags(log.Lmsgprefix | log.LstdFlags)
	if debug {
		logger.Debug.Enable()
	}

	cfg := &Config{}
	if err := cfg.Load(); err != nil {
		return nil, err
	}

	var api *ipx.Client

	if len(cfg.Address) > 0 {
		var err error
		api, err = ipx.New(cfg.Address, cfg.APIKey)
		if err != nil {
			return nil, err
		}
	}

	config := hc.Config{
		Port:        "9898",
		Pin:         "05727609",
		SetupId:     setupID(),
		StoragePath: "./cache",
	}

	bridge := accessory.Info{
		Manufacturer:     "Wey",
		Model:            fmt.Sprintf("Gateway (%v)", runtime.GOOS),
		SerialNumber:     utils.MacAddress(),
		Name:             fmt.Sprintf("Wey-%s", config.SetupId),
		FirmwareRevision: version,
	}

	t, err := hc.NewIPTransport(config, accessory.NewBridge(bridge).Accessory, cfg.Accessories.List(api)...)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	return &Gateway{
		cfg:    cfg,
		t:      t,
		config: config,
		bridge: bridge,
		onlyUI: len(cfg.Address) == 0,
	}, nil
}

func (gw Gateway) Run() {
	fmt.Println("------------------------------ Wey ------------------------------")
	fmt.Printf("| WebUI port            : 80\n")
	if !gw.onlyUI {
		fmt.Printf("| Homekit port          : 9898\n")
	}
	fmt.Printf("| Model                 : %s\n", gw.bridge.Model)
	fmt.Printf("| Version               : %s (%s - %s)\n", gw.bridge.FirmwareRevision, commit, date)
	fmt.Printf("| Serial                : %s\n", gw.bridge.SerialNumber)
	fmt.Printf("| Pin                   : %s (%s)\n", formatPin(gw.config.Pin), gw.config.SetupId)
	fmt.Println("-----------------------------------------------------------------")

	ui := newUI(gw)

	hc.OnTermination(func() {
		logger.Info.Println("Stop in progress...")
		ui.Stop()

		if !gw.onlyUI {
			<-gw.t.Stop()
		}
	})

	if gw.onlyUI {
		ui.Start()
	} else {
		go ui.Start()
		gw.t.Start()
	}
}

func formatPin(v string) string {
	if len(v) == 8 {
		return fmt.Sprintf("%s-%s-%s", v[0:3], v[3:5], v[5:8])
	}

	return ""
}

func setupID() string {
	mac := utils.MacAddress()
	mac = strings.ReplaceAll(mac, ":", "")

	return mac[7:11]
}
